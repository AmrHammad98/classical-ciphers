from Files import *
from Ceaser import *
from Hill import *
from PlayFair import *
from Vernam import *
from Vigenere import *

# input files
ceaser_PT = "Input Files\Caesar\caesar_plain.txt"
hill_PT_1 = "Input Files/Hill/hill_plain_2x2.txt"
hill_PT_2 = "Input Files/Hill/hill_plain_3x3.txt"
playfair_PT = "Input Files/PlayFair/playfair_plain.txt"
vernam_PT = "Input Files/Vernam/vernam_plain.txt"
vigenere_PT = "Input Files/Vigenere/vigenere_plain.txt"

# output files
ceaser_out_1 = "Output Files\Ceaser_Cipher_Text_key_3.txt"
ceaser_out_2 = "Output Files\Ceaser_Cipher_Text_key_6.txt"
ceaser_out_3 = "Output Files\Ceaser_Cipher_Text_key_12.txt"
hill_out_1 = "Output Files\Hill_Cipher_Text_2x2.txt"
hill_out_2 = "Output Files\Hill_Cipher_Text_3x3.txt"
playfiar_out_1 = "Output Files\Playfair_CipherText rats.txt"
playfiar_out_2 = "Output Files\Playfair_CipherText archangel.txt"
vernam_out = "Output Files\Vernam_CipherText.txt"
vigenere_out_1 = "Output Files\Vigenere_CipherText_pie.txt"
vigenere_out_2 = "Output Files\Vigenere_CipherText_aether.txt"

# keys
km_2 = np.array([[5, 8], [17, 3]])                                                 # 2x2 key matrix for hill cipher
km_3 = np.array([[2, 4, 12], [9, 1, 6], [7, 5, 3]])                                # 3x3 key matrix for hill cipher
pf_key_1 = "rats"                                                                  # playfair key 1
pf_key_2 = "archangel"                                                             # playfair key 2
vernam_key = "spartans"                                                            # vernam key

if __name__ == '__main__':
    # read plain text
    ceaser_PT = file_read(ceaser_PT)

    hill_PT_1 = file_read(hill_PT_1)
    hill_PT_2 = file_read(hill_PT_2)

    playfair_PT = file_read(playfair_PT)

    # Ceaser encryption
    ceaser_CT_3 = encrypt_ceaser(ceaser_PT, 3)                                      # key = 3
    file_write(ceaser_CT_3, ceaser_out_1)

    ceaser_CT_3 = encrypt_ceaser(ceaser_PT, 6)                                      # key = 6
    file_write(ceaser_CT_3, ceaser_out_2)

    ceaser_CT_3 = encrypt_ceaser(ceaser_PT, 12)                                     # key = 12
    file_write(ceaser_CT_3, ceaser_out_3)

    # Hill Cipher encryption
    hill_CT_2 = encrypt_hill(hill_PT_1, 0, km_2)
    hill_CT_3 = encrypt_hill(hill_PT_2, 1, km_3)

    file_write(hill_CT_2, hill_out_1)
    file_write(hill_CT_3, hill_out_2)

    # PlayFair Cipher encryption
    playfiar_CT_1 = encrypt_playfair(playfair_PT, pf_key_1)
    playfiar_CT_2 = encrypt_playfair(playfair_PT, pf_key_2)

    file_write(playfiar_CT_1, playfiar_out_1)
    file_write(playfiar_CT_2, playfiar_out_2)

    # Vernam encryption
    vernam_CT = encrypt_vernam(vernam_PT, vernam_key)
    file_write(vernam_CT, vernam_out)

    # vigenere encryption
    vigenere_CT_1 = encrypt_vigenere(vigenere_PT, "pie", True)
    vigenere_CT_2 = encrypt_vigenere(vigenere_PT, "aether", False)

    file_write(vigenere_CT_1, vigenere_out_1)
    file_write(vigenere_CT_2, vigenere_out_2)