# a method to eliminate duplicates in a string
def elem_dupl(str):
    res = ""
    for i in range(len(str)):
        if str[i] not in res:
            res += str[i]
    return res

# a method to deduce unused chars
def deduce_unused(used):
    res = []
    for i in range(97, 123):
        # case j skip
        if i == 106:
            continue
        if chr(i) not in used:
            res.append(chr(i))
    return res

# a method to create 5x5 matrix used in encryption using a given key
def create_matrix(key):
    mat = [[0 for x in range(5)] for y in range(5)]         # key matrix
    used = []                                               # string of used letters
    unused = []

    # eleminate duplicated chars in key
    key = elem_dupl(key)

    k = 0                                                   # itterator for key
    flag = True                                             # flag to start deducing unused chars
    x = 0                                                   # itterator for unused
    # fill matrix
    for i in range(5):
        for j in range(5):
          # put key in matrix first
          if k < len(key):
              mat[i][j] = key[k]
              used.append(key[k])
              k += 1
          else:
              # Deduce unused chars
              if flag:
                  unused = deduce_unused(used)
                  flag = False

              # i and j are considered the same char
              if unused[x] == 'j':
                  continue
              else:
                  mat[i][j] = unused[x]
              x += 1
    return(mat)

# a method to divide plaintext into diagrams
def diagrams (PT):

    split_text = []                                         # array of strings that will hold diagrams
    x = list(PT)

    # replace duplicates with x
    j = 1
    for i in range(len(x) -1):
        if x[i] == x[j]:
            x[j] = 'x'
        j += 1
    PT = ''.join([str(elem) for elem in x])

    # divide text
    for i in range(0, len(PT), 2):
        split_text.append(PT[i: i+2])

    # append a string with x if not a diagram
    if len(split_text[len(split_text) - 1]) != 2:
        split_text[len(split_text) - 1] = split_text[len(split_text) - 1] + "x"

    # print(split_text)
    return split_text

# a method to return index of diagrams in 5x5 matrix
# chr = character from plaintext, KM = KEY matrix
def locate(chr, KM):

    # case if chr is j
    if chr == 'j':
        chr = 'i'

    # locate chr in array
    for i in range(5):
        for j in range(5):
            if chr == KM[i][j]:
                return i, j

# a method to encrypt using playfair cipher
def encrypt_playfair(PT, k):

    # indices for each char in diagrams
    i1 = 0                                                      # index for char 1 in diagram
    i2 = 0                                                      # index for char 2 in diagram

    CT = ""                                                     # cipher text to be returned
    z = ""

    # create key matrix
    KM = create_matrix(k)

    # covert to lower case if capitalized
    if PT.isupper():
        PT = PT.lower()

    for c in PT:
        if c == "\n":
            continue
        z += c
    PT = z

    # split to diagrams
    diag = diagrams(PT)

    # encrypt
    for i in range(len(diag)):
        str = diag[i]

        i1 = locate(str[0], KM)
        i2 = locate(str[1], KM)

        # case 2 chars in same row
        if i1[0] == i2[0]:
            CT += KM[i1[0]][(i1[1] + 1) % 5]
            CT += KM[i2[0]][(i2[1] + 1) % 5]

        # case 3 chars in same column
        elif i1[1] == i2[1]:
            CT += KM[(i1[0] + 1) % 5][i1[1]]
            CT += KM[(i2[0] + 1) % 5][i2[1]]

        # otherwise
        else:
            CT += KM[i1[0]][i2[1]]
            CT += KM[i2[0]][i1[1]]
    return CT