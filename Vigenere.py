# a method to encrypt using vigenere cipher
def encrypt_vigenere(PT, k, mode):
    key = ""
    CT = ""

    # generate key

    # auto mode
    if mode:
        q = 0
        r = 0
        for i in PT:
            if q > len(k) - 1:
                key += PT[r]
                r += 1
            else:
                key += k[q]
                q += 1
        #print(key)
    # repetitive mode
    else:
        q = 0
        for i in PT:
            if q > len(k) - 1:
                q = 0
            key += k[q]
            q += 1
        #print(key)

    # start encryption
    for i in range(len(PT)):
        x = ord(PT[i]) - 97

        x += ord(key[i]) - 97

        x = x % 26

        CT += chr(x+97)

    return CT