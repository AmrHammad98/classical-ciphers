# a method to encrypt using vernam cipher
def encrypt_vernam(PT, k):
    j = 0
    CT = ""
    z = ""
    # exclude new lines from plain text
    for c in PT:
        if c == "\n":
            continue
        z += c
    PT = z

    # covert to lower case if capitalized
    if PT.isupper():
        PT = PT.lower()

    for i in PT:
        x = (ord(i) -97 ^ ord(k[j % 1]))
        CT += chr(x % 26)
        print(CT)
        j += 1
        if j>= len(k):
            j = 0
    return CT