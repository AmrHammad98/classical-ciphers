# a method to read from a text file and returns a string of the file content
# path: path of input file
def file_read(path):
    fr = open(path, "r")
    plaintext = fr.read()
    fr.close()
    return plaintext

# a method to write output in a text file
def file_write(str, path):
    fr = open(path, "w")
    if type(str) == type(list()):
        for x in str:
            fr.write(x)
    else:
        fr.write(str)
    fr.close()