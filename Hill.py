import numpy as np

# a method to encrypt using hill cipher
# pt = plaintext , option = 0 -> 2x2 , option =1 3x3, km -> keu matrix

def encrypt_hill(PT, option, km):

    aarr = []                   # array of asciis
    CT = ""                     # cipher text
    z = ""

    # covert to lower case if capitalized
    if PT.isupper():
        PT = PT.lower()

    # exclude new lines from plain text
    for c in PT:
        if c == "\n":
            continue
        z += c
    PT = z

    # creating key matrix
    if option == 0:
        # if PT length is odd make it even
        if len(PT) % 2 == 1:
            PT += 'x'

        # convert plaintext to asccii
        for c in PT:
            aarr.append(ord(c) - 97)

        # split plaintext to chunks of length 2
        y = np.array_split(aarr, len(aarr)/2)

        for i in y:
            # multiply by key matrix
            z = i.dot(km)

            # get mod 26
            for c in z:
                c = c % 26 + 97
                CT += chr(c)

    elif option == 1:
        # if PT length is even make it odd
        if len(PT) % 2 == 0:
            PT += 'x'

        # convert plaintext to asccii
        for c in PT:
            aarr.append(ord(c) - 97)

        # split plaintext to chunks of length 3
        y = np.array_split(aarr, len(aarr)/3)

        for i in y:
            # multiply by key matrix
            z = i.dot(km)

            # get mod 26
            for c in z:
                c = c % 26 + 97
                CT += chr(c)
    return CT




