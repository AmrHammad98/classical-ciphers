# a  method to encrypt plaintext using Ceaser Cipher
# PT = Plaintext , k = Key
def encrypt_ceaser(PT, k):
    cipher = ""
    for c in PT:
        if c == "\n":
            continue
        cipher += chr((ord(c) + k - 97) % 26 + 97)                        # 97 -> ascii code of "a"
    return cipher

# a method to decrypt a ceaser ciphertext
def decrypt_ceaser(CT, k):
    for c in CT:
        c = chr((ord(c) - k - 97) % 26 + 97)
        print(c)